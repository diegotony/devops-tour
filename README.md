# DevOps-Tour
Bienvenidos developers, a continuación se detalla las principales consideraciones para construir un flujo de trabajo DevOps. 

[1. Consideraciones](https://gitlab.com/diegotony/devops-tour/wikis/1.-Consideraciones)

[2. Flujo de Trabajo](https://gitlab.com/diegotony/devops-tour/wikis/2.-Flujo-de-Trabajo)

[3. Plataformas a Ocupar](https://gitlab.com/diegotony/devops-tour/wikis/3.-Plataformas-a-Ocupar)

[4. Proceso](https://gitlab.com/diegotony/devops-tour/wikis/4.-Proceso)